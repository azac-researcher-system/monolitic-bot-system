# Monolitic Bot System

---
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

**Summary:** the program is web crawler for some sources. The program can search with key words and scraping web content.

## Using

 - **as a Service with RestAPI**
   - [the endpoint documents](docs/using/restapi.md)
 - **Library**
   - [the function documents](docs/using/library.md)

## Dependency

### Language

 - **Python version** : 3.9

### Library

#### Python
 - **Selenium**
 - **requests**
 - **BeautifulSoup4**
 - **pika**
 - **json**
 - **flask**

### Tools

 - curl

## Project Structure

---

### Folders

 - **bots** : the bots
 - **control_system** : modules that are not bots in this program
 - **docs** : just markdown documents and some example file, 

### Files

 - **.env** : environment variables, the file using Dockerfile,if you don`t set the env, the program is use default variables from control_system/settings.py
 - **Dockerfile** : use this program on container, you can set published port 
 - **entrypoint.sh** : the file can set and run program

### General Algorithm

![algorithm](docs/pics/general-algorithm.png)

