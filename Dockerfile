FROM python:3.9-alpine3.16

COPY . /monolithic_bot_system
WORKDIR /monolithic_bot_system

RUN pip install -r requirements.txt
ENV DIRPATH=.env

CMD ["sh", "entrypoint.sh"]