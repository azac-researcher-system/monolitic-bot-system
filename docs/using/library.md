# Library Document

---

the document is explain some modules and bots libraries.

## The Bots

 - [Articles](../../bots/articles/README.md) : the bots can scraping some article websites.
   - [TutorialsPoint](https://www.tutorialspoint.com/)
   - [GeeksforGeeks](https://www.geeksforgeeks.org/)
 - [Forums](../../bots/forum/README.md)
   - [StackOverflow](https://stackoverflow.com/)
 - [Google](../../bots/google/README.md)
   - [Google](https://www.google.com/)
 - [News](../../bots/news/README.md)
   - [Hacker News](https://news.ycombinator.com/)
   - [The Hacker News](https://thehackernews.com/) 
 - [Scholar](../../bots/scholar/README.md)
   - [Google Scholar](https://scholar.google.com/)

## Control System

contains important functions other than bots in the [control system](../../control_system/README.md) file.

 - **key_words_checker.py** : This section first checks an already run word of mine before the bots run the keyword.
 - **bot_data_parser.py** : It is a module that parses and organizes all texts before the received bot data is sent to rabbitmq.
 - **pika_writer.py** : Data processed by bot_data_parser is sent here, where it is converted to json and written to the rabbitmq queue.
 - **settings.py** : environment variables, eg port, debug, rabbitmq connection information

## Outbound JSON Payload information for RabbitMQ

### Examples
```json
{
    "Title": "Microsoft Data Breaches: Full Timeline Through 2022",
    "url": "https://firewalltimes.com/microsoft-data-breach-timeline/",
    "Summary": "The most recent Microsoft breach occurred on March 20, 2022, when the hacker group Lapsus$ announced on Telegram that they had breached the company. Several Microsoft projects, including Bing and Cortana, were compromised in the incident. As far as we can tell, however, no customer data appears to have been exposed.",
    "KeyWord": "data leak",
    "BotType": "news"
}
```

