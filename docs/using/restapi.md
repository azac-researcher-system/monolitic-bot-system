# RestAPI Document

---

the document about how to use restapi of monolithic bot system.

## Using

---

### Default Information

 - Port number : 17080
 - RabbitMQ Queue : data

### Json payload syntax for POST

```json
{
    "BotType":"google",
    "KeyWords":"linux"
}
```

