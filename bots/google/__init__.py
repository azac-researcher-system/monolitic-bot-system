import requests
from bs4 import BeautifulSoup
from datetime import datetime
from control_system.bot_data_parser import bot_data_parser

class bot_google():
    def __init__(self,KeyWord):

        now = datetime.now()
        slug = f'{now.strftime("%Y%m%d%H%M%S")}'
        url = f'https://www.google.com/search?q={KeyWord}'
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
        response = requests.get(url, headers=headers)
        html = response.text

        soup = BeautifulSoup(html, 'html.parser')
        Content = ""
        results = soup.find_all('div', class_='g')

        for result in results:
            title = result.find('h3').text
            link = result.find('a').get('href')
            result = f"<h4>{title}</h4><a>{link}</a><br><br>"
            Content = Content + str(result)

        bot_data_parser(KeyWord+" Google results",slug,Content,KeyWord,"Google")
