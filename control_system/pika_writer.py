import json
import pika

from control_system.settings import SETTINGS

class pika_writer():
    """pika_writer : class rabbitmq writer
    Usage : pika_writer(Title,url,Summary,KeyWord,BotType)
    """
    def __init__(self,Title,slug,Content,KeyWord,BotType):
        """
        :param Title:
        :param slug:
        :param Summary:
        :param KeyWord:
        :param BotType:
        """
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=SETTINGS['RABBITMQ_HOST'],
                port=SETTINGS['RABBITMQ_PORT'],
                credentials=pika.PlainCredentials(
                    username=SETTINGS['RABBITMQ_USERNAME'],
                    password=SETTINGS['RABBITMQ_PASSWORD']
                )
            )
        )
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=SETTINGS['RABBITMQ_QUEUE'])

        data = {
            "Title": Title,
            "slug": slug,
            "Content": Content,
            "KeyWord": KeyWord,
            "BotType": BotType
        }

        self.send_to_queue(data)

        self.connection.close()

    def send_to_queue(self,data):
        data_json = json.dumps(data)

        self.channel.basic_publish(
            exchange="",
            routing_key=SETTINGS['RABBITMQ_QUEUE'],
            body=data_json
        )
        print("Sent to queue:", data_json)

