from os import environ


SETTINGS = {

    'SHARED': environ.get('SHARED', '127.0.0.1'),
    'API_PORT': environ.get('API_PORT','17080'),
    'DEBUG': environ.get('DEBUG','False'),

    'RABBITMQ_HOST' : environ.get('RABBITMQ_HOST','192.168.122.11'),
    'RABBITMQ_PORT': environ.get('RABBITMQ_PORT','5672'),
    'RABBITMQ_USERNAME': environ.get('RABBITMQ_USERNAME','queryvs'),
    'RABBITMQ_PASSWORD': environ.get('RABBITMQ_PASSWORD','queryvs'),
    'RABBITMQ_QUEUE': environ.get('RABBITMQ_QUEUE','data')
}