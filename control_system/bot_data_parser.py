from control_system.pika_writer import pika_writer

class bot_data_parser():
    def __init__(self,Title,slug,Content,KeyWord,BotType):
        """
        bot_data_parser() : bot data parse and edit function
        Parameters :
            :param Title:
            :param slug: slug is url post endpoint
            :param Summary:
            :param KeyWord:
            :param BotType:
        """
        # ////////////////////////////////////////////
        # this block for parse and edit
        # ////////////////////////////////////////////
        pika_writer(Title,slug,Content,KeyWord,BotType)