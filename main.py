import threading
import json

from flask import Flask, request

from bots.google import bot_google
from bots.news import bot_news
from bots.scholar import bot_scholar
from bots.forum import bot_forum
from bots.articles import bot_articles

from control_system.key_words_checker import key_words_checker
from control_system.settings import SETTINGS

app = Flask(__name__)

class connection_bots:
    """
    connection_bots() : class
        functions of this class are used to start bots.
    functions :
        conection_googleBot(keryword)   :  start google bot with keyword bots/google
        conection_articleBot(keryword)  :  start articles bot with keyword bots/articles
        conection_forumBot(keryword)    :  start forum bot with keyword bots/forum
        conection_newsBot(keryword)     :  start news bot with keyword bots/news
        conection_scholarBot(keryword)  :  start scholar bot with keyword bots/scholar
    """
    def __init__(self):
        pass
    def conection_googleBot(self, keyword):
        print("conection_googleBot")
        bot_google(keyword) # bot is starting web scraping
        print("thread message : ",keyword,"\nThread ID:", threading.get_ident())

    def conection_articleBot(self, keyword):
        print("conection_articleBot")
        bot_articles(keyword) # bot is starting web scraping
        print("thread message : ",keyword,"\nThread ID:", threading.get_ident())

    def conection_forumBot(self, keyword):
        print("conection_forumBot")
        bot_forum(keyword) # bot is starting web scraping
        print("thread message : ",keyword,"\nThread ID:", threading.get_ident())

    def conection_newsBot(self, keyword):
        print("conection_newsBot")
        bot_news(keyword) # bot is starting web scraping
        print("thread message : ",keyword,"\nThread ID:", threading.get_ident())

    def conection_scholarBot(self, keyword):
        print("conection_scholarBot")
        bot_scholar(keyword) # bot is starting web scraping
        print("thread message : ",keyword,"\nThread ID:", threading.get_ident())


def create_thread(function_name, BotType, KeyWords):
    """
    :param function_name :  connection_bots class bot function
    :param BotType       :  what is bot name? e.g google,articles,news
    :param KeyWords      :  keywords for web scraping
    :return              :  True or False. if created thread for bot, return True
    """
    # key_words_checker(BotType, KeyWords).check variable is return True or False.
    # Check if this load has been run before. this information will be saved in a json file for now.
    if (key_words_checker(BotType, KeyWords).check == True):
        # create thread for run bot
        thread = threading.Thread(target=function_name, args=(KeyWords,))
        thread.start()

        return True
    else:
        return False


# the endpoint get payload json and start bots used env or default port
@app.route('/api/payload', methods=['POST'])
def get_payload():
    bot_worked = False

    data = request.get_json() # get payload json from endpoint
    data_str = json.dumps(data) # convert string types
    parsed_data = json.loads(data_str) # parse json data

    # read variables
    BotType = parsed_data["BotType"]
    KeyWords = parsed_data["KeyWords"]


    print("runned if api endpoint")
    cb = connection_bots() # bot connection classes

    if (BotType == "google"):
        bot_worked = create_thread(cb.conection_googleBot, BotType, KeyWords)

    elif (BotType == "article"):
        bot_worked = create_thread(cb.conection_articleBot, BotType, KeyWords)

    elif (BotType == "scholar"):
        bot_worked = create_thread(cb.conection_scholarBot, BotType, KeyWords)

    elif (BotType == "news"):
        bot_worked = create_thread(cb.conection_newsBot, BotType, KeyWords)

    elif (BotType == "forum"):
        bot_worked = create_thread(cb.conection_forumBot, BotType, KeyWords)

    else:
        bot_worked = False
        print("wrong payload")

    if (bot_worked == True):
        print("started process")

    else:
        print("not started process")

    return data_str

if __name__ == '__main__':
    app.run(host=SETTINGS['SHARED'], debug=SETTINGS['DEBUG'], port=SETTINGS['API_PORT'])

else:
    from gevent.pywsgi import WSGIServer

    http_server = WSGIServer(('',int(SETTINGS['API_PORT'])), app, spawn=1000, log=None)
    http_server.serve_forever()
